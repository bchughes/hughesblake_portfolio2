﻿using System;
using System.Collections.Generic;
using System.Collections;


namespace HughesBlake_CodingExcercise3
{
    //creating a seperate class to make students
    public class Student
    {
        string fName;
        string lName;
        double GPA;

        /*  89.5 - 100 = A
            79.5 - 89.4 = B
            72.5 - 79.4 = C
            69.5 - 72.4 = D
            0 - 69.4 = F
            */


        //dictionary
        Dictionary<string, double> classes;

        public Student()
        {
            

        }

        //allowing the student class to be used in the main program
        public Student(string _fName, string _lName, Dictionary<string, double> _classes)
        {
            fName = _fName;
            lName = _lName;
            classes = _classes;

            GPA = FindGPA();
        }

        //new method to find student GPA
        private double FindGPA() 
        {
            //conditional to make sure grades aren't null
            if (classes != null)
            {
                string[] letterGrades = new string[10];
                int i = 0;


                foreach(KeyValuePair<string, double> grade in classes)
                {
                    letterGrades[i] = getLetterGrade(grade.Value);
                    i++;
                }

                i = 0;
                int totalGPA = 0;

                foreach(string value in letterGrades)
                {
                    switch (value)
                    {
                        case "A":
                            totalGPA += 4;
                            i += 1;
                            break;

                        case "B":
                            totalGPA += 3;
                            i += 1;
                            break;

                        case "C":
                            totalGPA += 2;
                            i += 1;
                            break;

                        case "D":
                            i += 1;
                            totalGPA += 1;
                            break;

                        case "F":
                            i += 1;
                            totalGPA += 0;
                            break;

                        default:
                            break;
                    }

                    return totalGPA / i;
                }
            }
            else
            {
                return 0.0;
            }
            return 0.0;
        }

        private string getLetterGrade(double numberGrade)
        {
            if (numberGrade <= 100 && numberGrade > 89.5)
            {
                return "A";
            }
            else if (numberGrade <= 89.4 && numberGrade > 79.5)
            {
                return "B";
            }
            else if (numberGrade <= 79.4 && numberGrade > 72.5)
            {
                return "C";
            }
            else if (numberGrade <= 72.4 && numberGrade > 69.5)
            {
                return "D";
            }
            else if (numberGrade <= 69.4 && numberGrade > 0)
            {
                return "F";
            }
            else
            {
                return "Invalid input.";
            }
        }
    }
}
