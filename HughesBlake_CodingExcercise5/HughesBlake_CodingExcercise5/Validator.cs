﻿using System;
namespace HughesBlake_CodingExcercise5
{
    abstract class Validator
    {
        public static string StringCheck()
        {
            while (true)
            {
                //make sure user enters the proper words, if typo or null, try again
                string userInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(userInput))
                {
                    if (userInput == "rock" || userInput == "paper" || userInput == "scissors" || userInput == "paper" || userInput == "lizard")
                    {
                        return userInput;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input.");
                    }
                }

                else
                {
                    Console.WriteLine("Invalid input.");
                }
            }
        }


        //validator to make sure the user enters properly, y loops, n breaks
        public static string Rematch()
        {
            while (true)
            {
                string userInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(userInput))
                {
                    if (userInput == "y" || userInput == "Y" || userInput == "n" || userInput == "N")
                    {
                        return userInput;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input.");
                    }
                }

                else
                {
                    Console.WriteLine("Invalid input.");
                }
            }
        }
    }
}
