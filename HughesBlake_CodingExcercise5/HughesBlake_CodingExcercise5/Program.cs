﻿using System;

namespace HughesBlake_CodingExcercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //DVP 2
            //Coding Excercise 6

            //Scissors beats paper, lizard
            //Rock beats scissors, lizard
            //Paper beats rock, spock
            //Lizard beats spock, paper
            //Spock beats scissors, rock

            //array of computer picks by random generator
            string[] comPicks = { "rock", "paper", "scissors", "lizard", "spock" };
            Random rand = new Random();

            //variables to keep track of stats
            int wins = 0;
            int ties = 0;
            int losses = 0;

            //make sure to ask the user to play over and over
            bool loop = true;

            //explain the rules
            Console.WriteLine("Welcom to 10 Matches of Rock, Paper, Scissors, Lizard, Spock. Here are the rules:");
            Console.WriteLine("- Scissors cuts Paper.");
            Console.WriteLine("- Paper covers Rock.");
            Console.WriteLine("- Rock crushes Lizard.");
            Console.WriteLine("- Lizard poisons Spock.");
            Console.WriteLine("- Spock smashes Scissors.");
            Console.WriteLine("- Scissors decapitates Lizard.");
            Console.WriteLine("- Lizard eats Paper.");
            Console.WriteLine("- Paper disproves Spock.");
            Console.WriteLine("- Spock vaporizes Rock.");
            Console.WriteLine("- Rock crushes Scissors.");

            //loops the game until user no longer wants to play
            while (loop)
            {
                //ask user to pick
                Console.WriteLine("Which do you choose: Rock, Paper, Scissors, Lizard, Spock?");
                string userInput = Validator.StringCheck().ToLower();
                //10 matches required
                for (int i = 0; i < 10; i++)
                {
                    //random computer pick
                    int indexOfRand = rand.Next(0, comPicks.Length);
                    switch (userInput)
                    {
                        //if user picks rock
                        case "rock":
                            Console.WriteLine("Computer picks " + comPicks[indexOfRand]);
                            //conditional to determine who wins, and track the stats
                            if (userInput == comPicks[indexOfRand])
                            {
                                ties++;
                            }
                            else if (comPicks[indexOfRand] == "paper" || comPicks[indexOfRand] == "spock")
                            {
                                losses++;
                            }
                            else if (comPicks[indexOfRand] == "scissors" || comPicks[indexOfRand] == "lizard")
                            {
                                wins++;
                            }
                            //displays total stats after every round
                            Console.WriteLine("Wins = " + wins + ", losses = " + losses + ", ties = " + ties + ".");
                            //prepares the next round
                            Console.WriteLine("Which do you choose: Rock, Paper, Scissors, Lizard, Spock?");
                            userInput = Validator.StringCheck().ToLower();
                            break;

                            //if user picks paper
                        case "paper":
                            Console.WriteLine("Computer picks " + comPicks[indexOfRand]);
                            if (userInput == comPicks[indexOfRand])
                            {
                                ties++;
                            }
                            else if (comPicks[indexOfRand] == "scissors" || comPicks[indexOfRand] == "lizard")
                            {
                                losses++;
                            }
                            else if (comPicks[indexOfRand] == "rock" || comPicks[indexOfRand] == "spock")
                            {
                                wins++;
                            }
                            Console.WriteLine("Wins = " + wins + ", losses = " + losses + ", ties = " + ties + ".");
                            Console.WriteLine("Which do you choose: Rock, Paper, Scissors, Lizard, Spock?");
                            userInput = Validator.StringCheck().ToLower();
                            break;

                            //if user picks scissors
                        case "scissors":
                            Console.WriteLine("Computer picks " + comPicks[indexOfRand]);
                            if (userInput == comPicks[indexOfRand])
                            {
                                ties++;
                            }
                            else if (comPicks[indexOfRand] == "rock" || comPicks[indexOfRand] == "spock")
                            {
                                losses++;
                            }
                            else if (comPicks[indexOfRand] == "paper" || comPicks[indexOfRand] == "lizard")
                            {
                                wins++;
                            }
                            //shows stats and preps for next round
                            Console.WriteLine("Wins = " + wins + ", losses = " + losses + ", ties = " + ties + ".");
                            Console.WriteLine("Which do you choose: Rock, Paper, Scissors, Lizard, Spock?");
                            userInput = Validator.StringCheck().ToLower();
                            break;

                            //if user picks lizard
                        case "lizard":

                            //determines what computer picks and keeps track of stats
                            Console.WriteLine("Computer picks " + comPicks[indexOfRand]);
                            if (userInput == comPicks[indexOfRand])
                            {
                                ties++;
                            }
                            else if (comPicks[indexOfRand] == "rock" || comPicks[indexOfRand] == "scissors")
                            {
                                losses++;
                            }
                            else if (comPicks[indexOfRand] == "paper" || comPicks[indexOfRand] == "spock")
                            {
                                wins++;
                            }
                            Console.WriteLine("Wins = " + wins + ", losses = " + losses + ", ties = " + ties + ".");
                            Console.WriteLine("Which do you choose: Rock, Paper, Scissors, Lizard, Spock?");
                            userInput = Validator.StringCheck().ToLower();
                            break;

                            //if user picks spock
                        case "spock":

                            Console.WriteLine("Computer picks " + comPicks[indexOfRand]);

                            //tracks stats for user
                            if (userInput == comPicks[indexOfRand])
                            {
                                ties++;
                            }
                            else if (comPicks[indexOfRand] == "paper" || comPicks[indexOfRand] == "lizard")
                            {
                                losses++;
                            }
                            else if (comPicks[indexOfRand] == "rock" || comPicks[indexOfRand] == "scissors")
                            {
                                wins++;
                            }
                            Console.WriteLine("Wins = " + wins + ", losses = " + losses + ", ties = " + ties + ".");
                            Console.WriteLine("Which do you choose: Rock, Paper, Scissors, Lizard, Spock?");
                            userInput = Validator.StringCheck().ToLower();
                            break;

                        default:
                            Console.WriteLine("Please enter Rock, Paper, Scissors, Lizard, or Spock. What was entered is an invalid choice.");
                            break;
                    }
                }
                //total stats after 10 matches
                Console.WriteLine("Total wins = " + wins + ".");
                Console.WriteLine("Total losses = " + losses + ".");
                Console.WriteLine("Total ties = " + ties + ".");

                //ask user to go again
                Console.WriteLine("Rematch all 10 matches? 'Y' for yes, 'N' for no.");
                string userRematch = Validator.Rematch().ToLower();

                //breaks loop
                if (userRematch == "n")
                {
                    loop = false;
                }
            }
        }
    }
}
