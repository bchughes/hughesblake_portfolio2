﻿using System;
using System.Collections.Generic;

namespace HughesBlake_CodingExcercise4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //DVP 2
            //Coding Excercise 4
            /*
             * facts[1] = "Red is the color of love.";
            facts[2] = "Red is the color blood once it makes contact with oxygen.";
            facts[3] = "Red > Blue.";
            facts[4] = "Wisconsin beat Miami this year in the Orange Bowl";
            facts[5] = "My roommate's favorite color is orange.";
            facts[6] = "Crossbreeding green-brown canaries and then feeding them a diet of red peppers will turn them orange over time.";
            facts[7] = "Yellow is considered a happy color, but can also cause anxiety.";
            facts[8] = "Yellow is the color of a banana that's ready to be eaten.";
            facts[9] = "Yellow the name of a song by Coldplay.";
            facts[10] = "Green"
            */

            //create a string array for the facts about the colors
            string[] facts = { "Red is the color of love.", "Red is the color blood once it makes contact with oxygen.", "Red > Blue." };

            //create a dictionary to match the facts to a color and add the red facts to the dictionary
            Dictionary<string, string[]> colorFacts = new Dictionary<string, string[]>();
            colorFacts.Add("red", facts);

            //create facts about orange and add them to the orange dictionary
            facts = new string[] { "Wisconsin beat Miami this year in the Orange Bowl", "My roommate's favorite color is orange.", "Crossbreeding green-brown canaries and then feeding them a diet of red peppers will turn them orange over time." };
            colorFacts.Add("orange", facts);

            //create facts about yellow and add them to the yellow dictionary
            facts = new string[] { "Yellow is considered a happy color, but can also cause anxiety.", "Yellow is the color of a banana that's ready to be eaten.", "Yellow the name of a song by Coldplay." };
            colorFacts.Add("yellow", facts);

            //create facts about green and add them to the green dictionary
            facts = new string[] { "Green is the most used color for nightvision goggles.", "Santa originally wore a green suit.", "Green is the color of grass."};
            colorFacts.Add("green", facts);

            //create facts about blue and add them to the blue dictionary
            facts = new string[] { "The odds of finding a blue lobster are roughly 1 in 2 million.", "8% of the world's population have blue eyes.", "Bluebirds can't see the color blue."};
            colorFacts.Add("blue", facts);

            //create facts about indigo and add them to the indigo dictionary
            facts = new string[] { "Indigo is the brightest tone of the rainbow.", "Indigo dye is commonly used in denim.", "The inidgo plant originated from India."};
            colorFacts.Add("indigo", facts);

            //create facts about violet and add them to the violet dictionary
            facts = new string[] { "The color we draw as violet is not true violet, since violet can only be seen through lightwaves.", "Violet is associated with royalty and nobility.", "The most famous violet dye was called Tyrian Purple."};
            colorFacts.Add("violet", facts);

            //bool to for the loop
            bool exit = false;

            //user input
            Console.WriteLine("Please enter one of the following colors: Red, Orange, Yellow, Green, Blue, Indigo, Violet");
            string userInput = Validator.StringCheck().ToLower();

            //create a variable to choose random displayed facts
            Random rand = new Random();


            //loop till user wants to exit
            while (!exit)
            {
                //switch case for user to choose what they want.
                switch (userInput)
                {
                    //red case
                    case "red":
                        string key = "red";
                        if(colorFacts.ContainsKey(key))
                        {
                            if(colorFacts.TryGetValue(key, out facts))
                            {
                                int indexOfRand = rand.Next(0, facts.Length);
                                Console.WriteLine(facts[indexOfRand]);
                            }
                            Console.WriteLine("Enter in a following color: Red, Orange, Yellow, Green, Blue, Indigo, Violet if you wish to see information about that color, or Exit if you wish to exit.");
                            userInput = Validator.StringCheck().ToLower();
                        }
                        break;
                        //orange case
                    case "orange":
                        key = "orange";
                        if (colorFacts.ContainsKey(key))
                        {
                            if (colorFacts.TryGetValue(key, out facts))
                            {
                                int indexOfRand = rand.Next(0, facts.Length);
                                Console.WriteLine(facts[indexOfRand]);
                            }
                            Console.WriteLine("Enter in a following color: Red, Orange, Yellow, Green, Blue, Indigo, Violet if you wish to see information about that color, or Exit if you wish to exit.");
                            userInput = Validator.StringCheck().ToLower();
                        }
                        break;
                        //yellow case
                    case "yellow":
                        
                        key = "yellow";
                        if (colorFacts.ContainsKey(key))
                        {
                            if (colorFacts.TryGetValue(key, out facts))
                            {
                                int indexOfRand = rand.Next(0, facts.Length);
                                Console.WriteLine(facts[indexOfRand]);
                            }
                            Console.WriteLine("Enter in a following color: Red, Orange, Yellow, Green, Blue, Indigo, Violet if you wish to see information about that color, or Exit if you wish to exit.");
                            userInput = Validator.StringCheck().ToLower();
                        }
                        break;
                        //green case
                    case "green":
                        
                        key = "green";
                        if (colorFacts.ContainsKey(key))
                        {
                            if (colorFacts.TryGetValue(key, out facts))
                            {
                                int indexOfRand = rand.Next(0, facts.Length);
                                Console.WriteLine(facts[indexOfRand]);
                            }
                            Console.WriteLine("Enter in a following color: Red, Orange, Yellow, Green, Blue, Indigo, Violet if you wish to see information about that color, or Exit if you wish to exit.");
                            userInput = Validator.StringCheck().ToLower();
                        }
                        break;
                        //blue case
                    case "blue":
                        
                        key = "blue";
                        if (colorFacts.ContainsKey(key))
                        {
                            if (colorFacts.TryGetValue(key, out facts))
                            {
                                int indexOfRand = rand.Next(0, facts.Length);
                                Console.WriteLine(facts[indexOfRand]);
                            }
                            Console.WriteLine("Enter in a following color: Red, Orange, Yellow, Green, Blue, Indigo, Violet if you wish to see information about that color, or Exit if you wish to exit.");
                            userInput = Validator.StringCheck().ToLower();
                        }
                        break;
                        //indigo case
                    case "indigo":
                        
                        key = "indigo";
                        if (colorFacts.ContainsKey(key))
                        {
                            if (colorFacts.TryGetValue(key, out facts))
                            {
                                int indexOfRand = rand.Next(0, facts.Length);
                                Console.WriteLine(facts[indexOfRand]);
                            }
                            Console.WriteLine("Enter in a following color: Red, Orange, Yellow, Green, Blue, Indigo, Violet if you wish to see information about that color, or Exit if you wish to exit.");
                            userInput = Validator.StringCheck().ToLower();
                        }
                        break;
                        //violet
                    case "violet":
                        
                        key = "violet";
                        if (colorFacts.ContainsKey(key))
                        {
                            if (colorFacts.TryGetValue(key, out facts))
                            {
                                int indexOfRand = rand.Next(0, facts.Length);
                                Console.WriteLine(facts[indexOfRand]);
                            }
                            Console.WriteLine("Enter in a following color: Red, Orange, Yellow, Green, Blue, Indigo, Violet if you wish to see information about that color, or Exit if you wish to exit.");
                            userInput = Validator.StringCheck().ToLower();
                        }
                        break;
                        //breaks loop
                    case "exit":
                        exit = true;
                        break;
                        //in case something is typed in wrong 
                    default:
                        Console.WriteLine("Invalid input. Please make sure to spell the colors correctly. (Red, Orange, Yellow, Green, Blue, Indigo, Violet)");
                        userInput = Validator.StringCheck().ToLower();
                        break;

                }
            }
        }
    }
}
